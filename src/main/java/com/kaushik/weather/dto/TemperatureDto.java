package com.kaushik.weather.dto;

public class TemperatureDto {
    private double temp_min;
    private double temp_max;
    private int humidity;
    private String main;

    public TemperatureDto(double temp_min, double temp_max, int humidity, String main) {
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.humidity = humidity;
        this.main = main;
    }

    public double getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(double temp_min) {
        this.temp_min = temp_min;
    }

    public double getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(double temp_max) {
        this.temp_max = temp_max;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    @Override
    public String toString() {
        return "TemperatureDto{" +
                "temp_min=" + temp_min +
                ", temp_max=" + temp_max +
                ", humidity=" + humidity +
                ", main='" + main + '\'' +
                '}';
    }
}
