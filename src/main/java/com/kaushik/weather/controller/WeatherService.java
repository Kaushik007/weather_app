package com.kaushik.weather.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaushik.weather.dto.TemperatureDto;
import com.kaushik.weather.model.List;
import com.kaushik.weather.model.Root;
import com.kaushik.weather.model.Weather;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;

@RequestMapping("/api")
@RestController
public class WeatherService {
    private String API="b6907d289e10d714a6e88b30761fae22";
    private final String url="https://samples.openweathermap.org/data/2.5/forecast/hourly?q=London,us&appid=b6907d289e10d714a6e88b30761fae22";

    @GetMapping("/maxTemparature")
    public String getDay() throws IOException {
        WeatherService ws=new WeatherService();
        Root root=ws.getRoot();
        double max=0;
        String date=null;
        for(List list:root.getList()){
            if(list.getMain().getTemp_max()>max){
                max=list.getMain().getTemp_max();
                date=list.getDt_txt();
            }
        }
        LocalDate localDate = LocalDate.of(Integer.parseInt(date.substring(0,4)), Integer.parseInt(date.substring(5,7))
                , Integer.parseInt(date.substring(8,10)));
        java.time.DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        return String.valueOf(dayOfWeek);
    }

    @GetMapping("/details/{date}")
    public java.util.List<TemperatureDto> getTemperatureDetails(@PathVariable(value = "date") String date) throws IOException {
        java.util.List<TemperatureDto> temperatureDtos=new ArrayList<>();
        WeatherService ws=new WeatherService();
        Root root=ws.getRoot();
        for(List list:root.getList()){
            if(date.equals(list.getDt_txt().substring(0,10))){
                for(Weather weather:list.getWeather()) {
                    TemperatureDto temperatureDto = new TemperatureDto(list.getMain().getTemp_min(),
                            list.getMain().getTemp_max(), list.getMain().getHumidity(), weather.getMain());
                    assert temperatureDtos != null;
                    temperatureDtos.add(temperatureDto);
                }
            }
        }
        return  temperatureDtos;
    }

    @GetMapping("/listOfRainyDates")
    public java.util.Set<String> getDates() throws IOException {
        java.util.Set<String> dates=new HashSet<>();
        WeatherService ws=new WeatherService();
        Root root=ws.getRoot();
        for(List list:root.getList()){
            for(Weather weather:list.getWeather()) {
                if(weather.getMain().equalsIgnoreCase("rain")){
                   dates.add(list.getDt_txt().substring(0,10));
                }
            }
        }
        return dates;
    }

    private  Root getRoot() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(new URL(url), Root.class);
    }
}